
import clases.extras.creadas.MapaRumania;
import clases.extras.creadas.Ciudad;
import clases.extras.creadas.MapaCiudades;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EstadoCiudad implements Estado {
	
	private int heuristica = 0;
        private int costo = 0;
	private final Ciudad META = Ciudad.B;
	private Ciudad posicionActual;

	public EstadoCiudad(Ciudad posiciones)
	{
		posicionActual = posiciones;
		setHeuristica();
	}

	@Override
	public double determinarCosto()
	{
		return 1;
	}

	private void setHeuristica()
	{
            heuristica=1;
	}

	public int getHeuristica()
	{
		return heuristica;
	}

	private String copiarPosiciones(String estado)
	{
		return new String(estado);
	}

	@Override
	public List<Estado> generarSucesores()
	{
            MapaCiudades mc = MapaRumania.getInstance();
//            System.out.println("CIUDAD EVALUADA: " + posicionActual);
//            System.out.println("LISTA DE CUIDADES" + lsCiudades);
            return convertirCiudadesAEstados(mc.obtenerAdyacentes(posicionActual));
	}
        
        private List<Estado> convertirCiudadesAEstados(List<Ciudad> lsCiudades){
            return lsCiudades.stream().map(e->new EstadoCiudad(e)).collect(Collectors.toCollection(ArrayList::new));
        }
        
	@Override
	public boolean esMeta()
	{
            return posicionActual == META;
	}

	@Override
	public void mostrarEstado()
	{
            System.out.println(posicionActual);
	}

	@Override
	public boolean igual(Estado s)
	{
            return posicionActual == ((EstadoCiudad) s).getPosicionActual();
            
	}

	public Ciudad getPosicionActual()
	{
		return posicionActual;
	}
        
        public double costoCambioEstado(EstadoCiudad otro){
//            int tam = this.getPosicionActual().length;
//            int dif,movidos=0;
//            double costoCambio=0.0;
//            for(int i = 0; i < tam-1; i++){
//                dif = this.getPosicionActual()[i] - otro.getPosicionActual()[i];
//                if(dif<0)
//                    movidos = movidos + (-1)*dif;
//                else
//                    movidos = movidos + dif;
//            }
//            if (movidos == 2)
//                costoCambio = 1.0;
//            else if (movidos == 1)
//                costoCambio = 2.0;
//            else
//                costoCambio = 0.0;

            return MapaRumania.getInstance().obtenerCostoViaje(posicionActual, otro.getPosicionActual());
        }    
        
            @Override
    public String toString(){
        return posicionActual + "";
    }
}
