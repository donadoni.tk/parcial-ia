package clases.extras.creadas;


import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miguel
 */
public interface MapaCiudades {
                
    List<Ciudad> obtenerAdyacentes(Ciudad ciudad);
    
    Integer obtenerCostoViaje(Ciudad origen, Ciudad destino);
}
