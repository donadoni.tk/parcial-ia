package clases.extras.creadas;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Miguel
 */
public class MapaRumania implements MapaCiudades {

    private static MapaCiudades mapaCiudades = new MapaRumania();

    public static MapaCiudades getInstance() {
        return mapaCiudades;
    }

    private Map<Ciudad, List<Ciudad>> grafoCiudades = new LinkedHashMap<>();
    private Map<Key, Integer> costosDeViaje = new LinkedHashMap<>();

    private MapaRumania() {
        grafoCiudades.put(Ciudad.A, Arrays.asList(Ciudad.S, Ciudad.T, Ciudad.Z));
        grafoCiudades.put(Ciudad.B, Arrays.asList(Ciudad.F, Ciudad.G, Ciudad.P, Ciudad.U));
        grafoCiudades.put(Ciudad.C, Arrays.asList(Ciudad.D, Ciudad.P, Ciudad.R));
        grafoCiudades.put(Ciudad.D, Arrays.asList(Ciudad.C, Ciudad.M));
        grafoCiudades.put(Ciudad.E, Arrays.asList(Ciudad.H));
        grafoCiudades.put(Ciudad.F, Arrays.asList(Ciudad.B, Ciudad.S));
        grafoCiudades.put(Ciudad.G, Arrays.asList(Ciudad.B));
        grafoCiudades.put(Ciudad.H, Arrays.asList(Ciudad.E, Ciudad.U));
        grafoCiudades.put(Ciudad.I, Arrays.asList(Ciudad.N, Ciudad.V));
        grafoCiudades.put(Ciudad.L, Arrays.asList(Ciudad.M, Ciudad.T));

        grafoCiudades.put(Ciudad.M, Arrays.asList(Ciudad.D, Ciudad.L));
        grafoCiudades.put(Ciudad.N, Arrays.asList(Ciudad.I));
        grafoCiudades.put(Ciudad.O, Arrays.asList(Ciudad.S, Ciudad.Z));
        grafoCiudades.put(Ciudad.P, Arrays.asList(Ciudad.B, Ciudad.C, Ciudad.R));
        grafoCiudades.put(Ciudad.R, Arrays.asList(Ciudad.C, Ciudad.P, Ciudad.S));
        grafoCiudades.put(Ciudad.S, Arrays.asList(Ciudad.A, Ciudad.F, Ciudad.O, Ciudad.R));
        grafoCiudades.put(Ciudad.T, Arrays.asList(Ciudad.A, Ciudad.L));
        grafoCiudades.put(Ciudad.U, Arrays.asList(Ciudad.B, Ciudad.H, Ciudad.V));
        grafoCiudades.put(Ciudad.V, Arrays.asList(Ciudad.I, Ciudad.U));
        grafoCiudades.put(Ciudad.Z, Arrays.asList(Ciudad.A, Ciudad.O));

        costosDeViaje.put(new Key(Ciudad.O, Ciudad.Z), 71);
        costosDeViaje.put(new Key(Ciudad.O, Ciudad.S), 151);
        costosDeViaje.put(new Key(Ciudad.Z, Ciudad.A), 75);
        costosDeViaje.put(new Key(Ciudad.A, Ciudad.S), 140);
        costosDeViaje.put(new Key(Ciudad.A, Ciudad.T), 118);
        costosDeViaje.put(new Key(Ciudad.T, Ciudad.L), 111);
        costosDeViaje.put(new Key(Ciudad.L, Ciudad.M), 70);
        costosDeViaje.put(new Key(Ciudad.M, Ciudad.D), 75);
        costosDeViaje.put(new Key(Ciudad.D, Ciudad.C), 120);
        costosDeViaje.put(new Key(Ciudad.C, Ciudad.R), 146);
        costosDeViaje.put(new Key(Ciudad.R, Ciudad.S), 80);
        costosDeViaje.put(new Key(Ciudad.S, Ciudad.F), 99);
        costosDeViaje.put(new Key(Ciudad.F, Ciudad.B), 211);
        costosDeViaje.put(new Key(Ciudad.R, Ciudad.P), 97);
        costosDeViaje.put(new Key(Ciudad.P, Ciudad.C), 138);
        costosDeViaje.put(new Key(Ciudad.P, Ciudad.B), 101);
        costosDeViaje.put(new Key(Ciudad.B, Ciudad.G), 90);
        costosDeViaje.put(new Key(Ciudad.B, Ciudad.U), 85);
        costosDeViaje.put(new Key(Ciudad.U, Ciudad.H), 98);
        costosDeViaje.put(new Key(Ciudad.H, Ciudad.E), 86);
        costosDeViaje.put(new Key(Ciudad.U, Ciudad.V), 142);
        costosDeViaje.put(new Key(Ciudad.V, Ciudad.I), 92);
        costosDeViaje.put(new Key(Ciudad.I, Ciudad.N), 87);
    }

    @Override
    public List<Ciudad> obtenerAdyacentes(Ciudad ciudad) {
        return grafoCiudades.get(ciudad);
    }

    @Override
    public Integer obtenerCostoViaje(Ciudad origen, Ciudad destino) {
        return costosDeViaje.get(new Key(origen, destino));
    }
    public static void main(String[] args) {
       
        System.out.println(getInstance().obtenerCostoViaje(Ciudad.I,Ciudad.V));
    }
}

class Key {

    private Ciudad origen;
    private Ciudad destino;

    public Key(Ciudad origen, Ciudad destino) {
        this.origen = origen;
        this.destino = destino;
    }

    public Ciudad getOrigen() {
        return origen;
    }

    public Ciudad getDestino() {
        return destino;
    }

    @Override
    public boolean equals(Object o) {
        
        if (this == o) {
            return true;
        }
        if (o instanceof Key) {
            Key key = (Key) o;

            if ((origen != null && key.origen != null && destino != null && key.destino != null)
                    && ((origen == key.origen && destino == key.destino) || (origen == key.destino && destino == key.origen))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    @Override
    public int hashCode() {

        return  Objects.hashCode(this.origen)+Objects.hashCode(this.destino);
    }
}
